# Kube-Prometheus 介绍

本项目集成了Kubernetes资源对象，[Grafana](http://grafana.com/)仪表板和[Prometheus规则](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/)，以及[Prometheus](https://prometheus.io/)通过使用Prometheus Operator提供了易于操作的、端到端的，监控Kubernetes集群的文档和脚本。

此软件包含的组件:

* The [Prometheus Operator](https://github.com/coreos/prometheus-operator)
* Highly available [Prometheus](https://prometheus.io/)
* Highly available [Alertmanager](https://github.com/prometheus/alertmanager)
* [Prometheus node-exporter](https://github.com/prometheus/node_exporter)
* [Prometheus Adapter for Kubernetes Metrics APIs](https://github.com/DirectXMan12/k8s-prometheus-adapter)
* [kube-state-metrics](https://github.com/kubernetes/kube-state-metrics)
* [Grafana](https://grafana.com/)

## Operator
Operator是由 CoreOS 公司开发的，用来扩展 Kubernetes API，特定的应用程序控制器。它被用来创建、配置和管理复杂的有状态应用，如数据库、缓存和监控系统。Operator 是基于 Kubernetes 的资源和控制器概念之上构建，但同时又包含了应用程序特定的一些专业知识：比如创建一个数据库的Operator，则必须对创建的数据库的各种运维方式非常了解，创建Operator的关键是CRD（自定义资源）的设计。

>注释：<br>
CRD是对 Kubernetes API 的扩展，Kubernetes 中的每个资源都是一个 API 对象的集合，例如我们在 YAML文件里定义的那些spec都是对 Kubernetes 中的资源对象的定义，所有的自定义资源可以跟 Kubernetes 中内建的资源一样使用 kubectl 操作。

Operator是将运维人员对软件操作的知识给代码化，同时利用 Kubernetes 强大的抽象来管理大规模的软件应用。目前CoreOS官方提供了几种Operator的实现，其中就包括我们今天的主角：Prometheus Operator，Operator的核心实现就是基于 Kubernetes 的以下两个概念：

* 资源：对象的状态定义；
* 控制器：观测、分析和行动，以调节资源的分布。

在最新的版本中，Kubernetes的 Prometheus-Operator 部署内容已经从 Prometheus-Operator 的 Github工程中拆分出独立工程Kube-Prometheus。Kube-Prometheus 即是通过 Operator 方式部署的Kubernetes集群监控，所以我们直接容器化部署 Kube-Prometheus 即可。

## Kubernetes 兼容性列表

| Kube-Prometheus stack |   K8S 1.16   | K8S 1.17 | K8S 1.18 | K8S 1.19 | K8S 1.20 |
|-----------------------|:------------:|:--------:|:--------:|:--------:|:--------:|
| `Release-0.4`         | ✔ (v1.16.5+) | ✔        | ✗        | ✗        | ✗        |
| `Release-0.5`         | ✗            | ✗        | ✔        | ✗        | ✗        |
| `Release-0.6`         | ✗            | ✗        | ✔        | ✔        | ✗        |
| `Release-0.7`         | ✗            | ✗        | ✗        | ✔        | ✔        |
| `HEAD`                | ✗            | ✗        | ✗        | ✔        | ✔        |

**注意**：由于之前的Kubernetes v1.16.和v1.16.5版本的两个Bug，Kube-Prometheus Release-0.4分支版本仅支持K8S v1.16.5和更高版本。可以手动编辑kube-system命名空间中的`extension-apiserver-authentication-reader`角色，赋予list和watch权限，以便解决Kubernetes v1.16.2至v1.16.4的第二个Bug。

以官方Kube-Prometheus Release-0.7 版本为例，涉及的镜像有以下组件
- quay.io/prometheus-operator/prometheus-config-reloader:v0.45.0
- quay.io/prometheus-operator/prometheus-operator:v0.45.0
- quay.io/brancz/kube-rbac-proxy:v0.8.0
- quay.io/prometheus/prometheus:v2.24.0
- quay.io/prometheus/alertmanager:v0.21.0
- quay.io/coreos/kube-state-metrics:v1.9.7
- quay.io/prometheus/node-exporter:v1.0.1
- directxman12/k8s-prometheus-adapter:v0.8.2
- grafana/grafana:7.3.5
>**提示**：<br>为了实现镜像加速，本项目中YAML资源文件中的`image`镜像地址已经修改为阿里云的。

## 克隆项目仓库至/opt目录
```
git clone https://gitlab.com/snight/kube-prometheus.git /opt/
```

# 配置 Kube-Prometheus

## NodePort端口
|   service  |  port |nodePort|
|------------|:-----:|:------:|
|prometheus  |  9090 |  30090 |
|alertmanager|  9093 |  30093 |
|grafana     |  3000 |  30000 |

> 如果出现端口冲突的问题，可以手动修改下面的三个文件中的nodePort
> * /opt/kube-prometheus/manifests/prometheus-service.yaml
> * /opt/kube-prometheus/manifests/alertmanager-service.yaml
> * /opt/kube-prometheus/manifests/grafana-service.yaml

## prometheus存储时长
```shell
vim /opt/kube-prometheus/manifests/setup/prometheus-operator-deployment.yaml
```
```yaml
apiVersion: monitoring.coreos.com/v1
kind: Prometheus
metadata:
  labels:
    app.kubernetes.io/component: prometheus
    app.kubernetes.io/name: prometheus
    app.kubernetes.io/part-of: kube-prometheus
    app.kubernetes.io/version: 2.24.0
    prometheus: k8s
  name: k8s
  namespace: monitoring
spec:
# ----添加retention参数配置----
  retention: 30d
  alerting:
    alertmanagers:
    - name: alertmanager-main
      namespace: monitoring
      port: web
...
```
## 配置Prometheus数据持久化
```shell
vim /opt/kube-prometheus/manifests/prometheus-prometheus.yaml
```
取消注释下面的内容，根据实际情况修改存储类名和容量大小
```yaml
# -------这部分为持久化配置-----------------
  storage:
    volumeClaimTemplate:
      spec:
        storageClassName: rook-ceph-block
        accessModes: ["ReadWriteOnce"]
        resources:
          requests:
            storage: 10Gi
# ---------------------------------------
```
## 配置Grafana数据持久化
```shell
vim /opt/kube-prometheus/manifests/grafana-deployment.yaml 
```
在YAML文件开头添加下面的内容，创建存储类。
```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: grafana-pvc
  namespace: monitoring
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 10Gi
  storageClassName: rook-ceph-block
---
```
然后修改emptydir存储方式改为PVC方式
```yaml
      volumes:
      # 注释掉emptyDir的定义
      # - emptyDir: {}
      #   name: grafana-storage
      # 下面重新定义grafana-storage属性
      - name: grafana-storage
        persistentVolumeClaim:
          claimName: grafana-pvc
```

## 部署安装 Kube-Prometheus
* 使用manifests目录中的配置创建监控套件
```shell
cd /opt/kube-prometheus
kubectl create -f manifests/setup
until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
kubectl create -f manifests/
```
首先创建名称空间和自定义资源对象，避免在部署监控组件时出现冲突。另外，两个文件夹中的资源都可以使用单独的命令来创建 `kubectl create -f manifests/setup -f manifests`，但是可能需要多次执行该命令才能成功安装所有组件。
* 删除监控套件
```shell
kubectl delete --ignore-not-found=true -f manifests/ -f manifests/setup
```

## 访问仪表板
Prometheus,Grafana和Alertmanager的仪表板通过运行下面的`kubectl port-forward`命令快速实现访问。它需要Kubernetes 1.10或更新的版本才能支持。

Prometheus
```shell
$ kubectl --namespace monitoring port-forward svc/prometheus-k8s 9090
```
访问 http://localhost:9090 。

Grafana
```shell
$ kubectl --namespace monitoring port-forward svc/grafana 3000
```
访问 http://localhost:3000 ,使用默认的账号/密码:admin/admin登录。

Alert Manager
```shell
$ kubectl --namespace monitoring port-forward svc/alertmanager-main 9093
```
访问 http://localhost:9093

## 修改kube-scheduler和kube-controller-manager服务绑定的IP地址
登录到Kubernetes主节点的服务器修改下面的文件，避免出现Kube-Prometheus无法监听kube-scheduler和kube-controller-manager服务的问题。
```shell
# 进入目录
cd /etc/kubernetes/manifests/

# 修改 scheduler 绑定的IP地址
# vim kube-scheduler.yaml

spec:
  containers:
  - command:
    - kube-scheduler
    - --bind-address=127.0.0.1 # 更改为0.0.0.0
    - --kubeconfig=/etc/kubernetes/scheduler.conf
    - --leader-elect=true

# 修改 controller-manager 绑定的IP地址
# vim kube-controller-manager.yaml
spec:
  containers:
  - command:
    - kube-controller-manager
    - --allocate-node-cidrs=true
    - --authentication-kubeconfig=/etc/kubernetes/controller-manager.conf
    - --authorization-kubeconfig=/etc/kubernetes/controller-manager.conf
    - --bind-address=127.0.0.1 # 更改为0.0.0.0
```

